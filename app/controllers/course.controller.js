const mongoose = require("mongoose");
const courseModel = require("../models/course.model");

const createCourse = (req, res) => {
    //B1: Thu thập dữ liệu
    var {
        reqTitle,
        reqDescription,
        reqStudent
    } = req.body;

    //B2: Validate dữ liệu
    if(reqTitle === undefined || reqTitle === "") {
        return res.status(400).json({
            status: "Bad Request",
            message: "Title is not valid!"
        })
    }
    // phủ định mệnh đề reqStudent phải là số nguyên và số lớn hơn hoặc bằng 0
    if(reqStudent !== undefined && !( Number.isInteger(reqStudent) && reqStudent >= 0 )) {
        return res.status(400).json({
            status: "Bad Request",
            message: "No student is not valid!"
        })
    }

    //B3: Xử lý bài toán
    //3.1 Chuẩn bị dữ liệu
    var createCourse = {
        _id: mongoose.Types.ObjectId(),
        title: reqTitle,
        description: reqDescription,
        noStudent: reqStudent
    }

    //3.2 Gọi Model để thêm dữ liệu
    courseModel.create(createCourse, (err, data) => {
        if(err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }

        return res.status(201).json({
            status: "Create course successfully",
            newCourse: data
        })
    })
}

const getAllCourse = (req, res) => {
    //B1: Thu thập dữ liệu
    //B2: Validate dữ liệu
    //B3: Xử lý bài toán
    //3.1 Chuẩn bị dữ liệu
    //3.2 Gọi Model để lấy dữ liệu
    courseModel.find((err, data) => {
        if(err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }

        return res.status(200).json({
            status: "Get courses successfully",
            courses: data
        })
    })
}

const getCourseById = (req, res) => {
    //B1: Thu thập dữ liệu
    const courseid = req.params.courseid;

    //B2: Validate dữ liệu
    if( !mongoose.Types.ObjectId.isValid(courseid) ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Course ID is not valid!"
        })
    }

    //B3: Xử lý bài toán
    //3.1 Chuẩn bị dữ liệu
    //3.2 Gọi Model để lấy dữ liệu
    courseModel.findById(courseid, (err, data) => {
        if(err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }

        if(data) {
            return res.status(200).json({
                status: "Get course successfully",
                course: data
            })
        } else {
            return res.status(404).json({
                status: "Not Found"
            })
        }
       
    })
}

const updateCourseById = (req, res) => {
    //B1: Thu thập dữ liệu
    const courseid = req.params.courseid;
    const {
        reqTitle,
        reqDescription,
        reqStudent
    } = req.body;

    //B2: Validate dữ liệu
    if( !mongoose.Types.ObjectId.isValid(courseid) ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Course ID is not valid!"
        })
    }

    if(reqTitle === "") {
        return res.status(400).json({
            status: "Bad Request",
            message: "Title is not valid!"
        })
    }
    // phủ định mệnh đề reqStudent phải là số nguyên và số lớn hơn hoặc bằng 0
    if(reqStudent !== undefined && !( Number.isInteger(reqStudent) && reqStudent >= 0 )) {
        return res.status(400).json({
            status: "Bad Request",
            message: "No student is not valid!"
        })
    }

    //B3: Xử lý bài toán
    //3.1 Chuẩn bị dữ liệu
    var updateCourse = {}

    if(reqTitle) updateCourse.title = reqTitle;

    if(reqDescription) updateCourse.description = reqDescription;

    if(reqStudent || reqStudent === 0) updateCourse.noStudent = reqStudent;

    //3.2 Gọi Model để cập nhật dữ liệu
    courseModel.findByIdAndUpdate(courseid, updateCourse, (err, data) => {
        if(err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }

        if(data) {
            return res.status(200).json({
                status: "Update course successfully",
                updatedCourse: data
            })
        } else {
            return res.status(404).json({
                status: "Not Found"
            })
        }
    })
}

const deleteCourseById = (req, res) => {
    //B1: Thu thập dữ liệu
    const courseid = req.params.courseid;

    //B2: Validate dữ liệu
    if( !mongoose.Types.ObjectId.isValid(courseid) ) {
        return res.status(400).json({
            status: "Bad Request",
            message: "Course ID is not valid!"
        })
    }

    //B3: Xử lý bài toán
    //3.1 Chuẩn bị dữ liệu
    //3.2 Gọi Model để xóa dữ liệu
    courseModel.findByIdAndDelete(courseid, (err, data) => {
        if(err) {
            return res.status(500).json({
                status: "Internal server error",
                message: err.message
            })
        }
         
        if(data) {
            return res.status(204).json();
        } else {
            return res.status(404).json({
                status: "Not Found"
            })
        }
    })
}

module.exports = {
    createCourse,
    getAllCourse,
    getCourseById,
    updateCourseById,
    deleteCourseById
}