//Import thư viện mongoose
const mongoose = require("mongoose");

// Khai báo class Schema
const Schema = mongoose.Schema;

// Khởi tạo review Schema
const reviewSchema = new Schema({
    stars: {
        type: Number,
        default: 0
    },
    note: {
        type: String,
        required: false
    }
}, {
    timestamps: true
});

// Biên dịch Review model từ Review Schema
module.exports = mongoose.model("Review", reviewSchema);